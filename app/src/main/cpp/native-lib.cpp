#include <string>

#include <EGL/egl.h>
#include <GLES3/gl3.h>

#include <android/sensor.h>
#include <android/native_window.h>
#include <android_native_app_glue.h>

EGLConfig config;
EGLSurface surface;
EGLContext context;
EGLDisplay display;

struct android_app *app;

static int InitDisplay()
{
    const EGLint attribs[] =
    {
         EGL_BAD_SURFACE, EGL_WINDOW_BIT,
         EGL_BLUE_SIZE, 8,
         EGL_GREEN_SIZE, 8,
         EGL_RED_SIZE, 8,
         EGL_NONE
    };

    EGLint w, h, format;
    EGLint numConfigs;

    display = eglGetDisplay(EGL_DEFAULT_DISPLAY);

    eglInitialize(display, 0, 0);
    eglChooseConfig(display, attribs, &config, 1, &numConfigs);
    eglGetConfigAttrib(display, config, EGL_NATIVE_VISUAL_ID, &format);

    ANativeWindow_setBuffersGeometry(app->window, 0, 0, format);

    surface = eglCreateWindowSurface(display, config, app->window, NULL);
    context = eglCreateContext(display, config, NULL, NULL);

    eglMakeCurrent(display, surface, surface, context);
    eglQuerySurface(display, surface, EGL_WIDTH, &w);
    eglQuerySurface(display, surface, EGL_HEIGHT, &h);

    glClearColor(0.5f, 0.5f, 0.f, 1.f);

    return 0;
}

void Draw()
{
    glClear(GL_COLOR_BUFFER_BIT);
}

void HandleAppCmd(struct android_app *app, int32_t cmd)
{
    switch(cmd)
    {
        case APP_CMD_INIT_WINDOW:
            InitDisplay();
            break;
    }
}

void android_main(struct android_app *state)
{
    int ident;
    int events;
    struct android_poll_source *source;

    app_dummy();

    state->onAppCmd = HandleAppCmd;
    app = state;

    while(1)
    {
        while((ident = ALooper_pollAll(0, NULL, &events, (void**)&source)) >= 0)
        {
            if(source)
            {
                source->process(state, source);
            }
        }

        Draw();
        eglSwapBuffers(display, surface);
    }
}